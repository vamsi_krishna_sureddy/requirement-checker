//
//  AddItemsViewController.swift
//  Project
//
//  Created by Student on 4/15/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit
import CoreData

class AddItemsViewController: UIViewController,UITextFieldDelegate {
    
    var moc : NSManagedObjectContext!
    @IBOutlet weak var itemNameTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addBTN(sender: AnyObject) {
        
        if itemNameTF.text == "" {
             UIAlertView(title: "Warning", message: "Please enter all feilds", delegate: nil, cancelButtonTitle: "Okay").show()
        }
        else {
        var activityRow = (UIApplication.sharedApplication().delegate as! AppDelegate).activityRow
        moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        var item : Item = NSEntityDescription.insertNewObjectForEntityForName("Item", inManagedObjectContext: moc) as! Item
        item.name = NSString(string:itemNameTF.text!).capitalizedString
        item.checked = false
        item.activity = (UIApplication.sharedApplication().delegate as! AppDelegate).activities[activityRow]
        var items : [Item] = (UIApplication.sharedApplication().delegate as! AppDelegate).activities[activityRow].items.allObjects as! [Item]
        items.append(item)
        (UIApplication.sharedApplication().delegate as! AppDelegate).activities[activityRow].items = NSSet(array: items)
        
        var error:NSError?
        do {
            try moc.save()
        } catch var error1 as NSError {
            error = error1
        }
        self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        
        self.view.endEditing(true)
    }
    
    @IBAction func cancelBTN(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
