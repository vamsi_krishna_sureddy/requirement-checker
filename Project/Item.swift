//
//  Item.swift
//  Project
//
//  Created by Student on 4/15/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import Foundation
import CoreData


class Item: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var checked : Bool
    @NSManaged var activity: NSManagedObject

}
