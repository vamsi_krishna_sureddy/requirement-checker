//
//  AddActivityViewController.swift
//  Project
//
//  Created by Student on 4/15/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit
import CoreData

class AddActivityViewController: UIViewController,UITextFieldDelegate {
    
    var managedObjectContext : NSManagedObjectContext!
    
    @IBOutlet weak var activityNameTF: UITextField!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var dayLBl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addActivity(sender: AnyObject) {
        var currentDate = NSDate()
        if activityNameTF.text == "" || currentDate.compare(timePicker.date) == NSComparisonResult.OrderedDescending {
            UIAlertView(title: "Warning", message: "Please enter valid data and select valid date", delegate: nil, cancelButtonTitle: "Okay").show()
         }
        else {
            
            var _date = timePicker.date
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "EEEE"
            var dayOfWeekString = dateFormatter.stringFromDate(_date)
            var dr = (UIApplication.sharedApplication().delegate as! AppDelegate).daysRow
            var selectedweek = (UIApplication.sharedApplication().delegate as! AppDelegate).days[dr].name
            if dayOfWeekString == selectedweek {
            var daysRow = (UIApplication.sharedApplication().delegate as! AppDelegate).daysRow
            managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
            
            var activity : Activity = NSEntityDescription.insertNewObjectForEntityForName("Activity", inManagedObjectContext: managedObjectContext) as! Activity
            activity.title = NSString(string: activityNameTF.text!).capitalizedString
            activity.time = timePicker.date
            activity.day = (UIApplication.sharedApplication().delegate as! AppDelegate).days[daysRow]
            
            var activities : [Activity] = (UIApplication.sharedApplication().delegate as! AppDelegate).days[daysRow].activities.allObjects as! [Activity]
            activities.append(activity)
            (UIApplication.sharedApplication().delegate as! AppDelegate).days[daysRow].activities = NSSet(array: activities)
            
            var error:NSError?
            do {
                try managedObjectContext.save()
            } catch var error1 as NSError {
                error = error1
            }
            
            
            
            self.dismissViewControllerAnimated(true, completion: nil)
            }
            else {
                UIAlertView(title: "Warning", message: "Please select appropriate weekDay", delegate: nil, cancelButtonTitle: "Okay").show()
            }
        }
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func cancleBTN(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewWillAppear(animated: Bool) {
        let daysRow = (UIApplication.sharedApplication().delegate as! AppDelegate).daysRow
        dayLBl.text = (UIApplication.sharedApplication().delegate as! AppDelegate).days[daysRow].name
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
