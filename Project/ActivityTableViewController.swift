//
//  ActivityTableViewController.swift
//  Project
//
//  Created by Student on 4/15/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit
import CoreData

class ActivityTableViewController: UITableViewController {

    var moc : NSManagedObjectContext!
    var act : [Activity] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

      //  self.tableView.backgroundColor = UIColor(patternImage: UIImage(named: "bg3.jpg")!)
        self.tableView.separatorColor = UIColor.blackColor()
        moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let fetch = NSFetchRequest(entityName: "Day")
        fetch.sortDescriptors = [NSSortDescriptor(key: "sortKey", ascending: true)]
        let days = (try! moc.executeFetchRequest(fetch))as! [Day]
        (UIApplication.sharedApplication().delegate as! AppDelegate).days = days
        let daysRow = (UIApplication.sharedApplication().delegate as! AppDelegate).daysRow
        act = (UIApplication.sharedApplication().delegate as! AppDelegate).days[daysRow].activities.allObjects as! [Activity]
        (UIApplication.sharedApplication().delegate as! AppDelegate).activities = act
        self.tableView.reloadData()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {    
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        return act.count
       
    }

  
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("activity", forIndexPath: indexPath) 
        var daysRow = (UIApplication.sharedApplication().delegate as! AppDelegate).daysRow
        cell.backgroundColor = UIColor(red: 1/255, green: 1/255, blue: 1/255, alpha: 0.0)
        
        let textTAG : Int = 100
        let detailTAG : Int = 200
        let title:UILabel = cell.viewWithTag(textTAG) as! UILabel!
        let detailText : UILabel = cell.viewWithTag(detailTAG) as! UILabel!
        
        if act.count != 0 {
            title.text = act[indexPath.row].title
            let curr = NSDate()
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateOfString = dateFormatter.stringFromDate(curr)
            
            let date : NSDate = act[indexPath.row].time
            let dateFormatter1 = NSDateFormatter()
            dateFormatter1.dateFormat = "yyyy-MM-dd"
            let dateOfString1 = dateFormatter1.stringFromDate(date)
            print(dateOfString)
            print(dateOfString1)
            
            if dateOfString == dateOfString1 {
                title.font = UIFont(name: "HoeflerText-Black", size: 20.0)
                title.textColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 0.8)
                detailText.textColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 0.4)
            }
            
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Hour, .Minute], fromDate: date)
            let hour = components.hour
            let minutes = components.minute
            
            let formatterDate = NSDateFormatter()
            formatterDate.dateStyle = .ShortStyle
            let dateString = formatterDate.stringFromDate(date)
            
            detailText.text = String(format: "Time - %d : %d On %@",hour,minutes,dateString)
            
            
        }
        return cell
     }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            moc.deleteObject(act[indexPath.row])
            act.removeAtIndex(indexPath.row)
            
            do {
                try moc.save()
            } catch _ {
            }
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        (UIApplication.sharedApplication().delegate as! AppDelegate).items = act[indexPath.row].items.allObjects as! [Item]
        
         (UIApplication.sharedApplication().delegate as! AppDelegate).participants = act[indexPath.row].participants.allObjects as! [Participant]
        
        (UIApplication.sharedApplication().delegate as! AppDelegate).activityRow = indexPath.row
        
    }
    
    
    
    override func viewWillAppear(animated: Bool) {
        let fetch = NSFetchRequest(entityName: "Day")
        fetch.sortDescriptors = [NSSortDescriptor(key: "sortKey", ascending: true)]
        let days = (try! moc.executeFetchRequest(fetch)) as! [Day]
        (UIApplication.sharedApplication().delegate as! AppDelegate).days = days
        
        let daysRow = (UIApplication.sharedApplication().delegate as! AppDelegate).daysRow
        self.navigationItem.title = (UIApplication.sharedApplication().delegate as! AppDelegate).days[daysRow].name
        act = (UIApplication.sharedApplication().delegate as! AppDelegate).days[daysRow].activities.allObjects as! [Activity]
        (UIApplication.sharedApplication().delegate as! AppDelegate).activities = act
        self.tableView.reloadData()
        
        for var i=0;i<act.count;i++ {
            let currentDate = NSDate()
            if currentDate.compare(act[i].time) == NSComparisonResult.OrderedDescending
            {
                
            }
            else {
                let localNotification:UILocalNotification = UILocalNotification()
                localNotification.alertAction = "Activities to-do Today"
                
                let date : NSDate = act[i].time
                let calendar = NSCalendar.currentCalendar()
                let components = calendar.components([.Hour, .Minute], fromDate: date)
                let hour = components.hour
                let minutes = components.minute
                
                let formatterDate = NSDateFormatter()
                formatterDate.dateStyle = .ShortStyle
                let dateString = formatterDate.stringFromDate(date)
                
                localNotification.alertBody = act[i].title + " - " + String(format: "Time - %d : %d On %@",hour,minutes,dateString)
                localNotification.fireDate = act[i].time
                UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
            }
        }
    }
    
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func cancel(segue: UIStoryboardSegue) {
        
    }
}
