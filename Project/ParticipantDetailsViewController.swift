//
//  ParticipantDetailsViewController.swift
//  Project
//
//  Created by Student on 4/16/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit
import CoreData
import MessageUI

class ParticipantDetailsViewController: UIViewController,MFMailComposeViewControllerDelegate {

    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    var moc : NSManagedObjectContext!
    var participants : [Participant] = []
    var row : Int = 0
    @IBOutlet weak var nameLBL: UILabel!
    @IBOutlet weak var emailLBL: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Contact Info"
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        let fetch = NSFetchRequest(entityName: "Day")
        fetch.sortDescriptors = [NSSortDescriptor(key: "sortKey", ascending: true)]
        let days = (try! moc.executeFetchRequest(fetch)) as! [Day]
        (UIApplication.sharedApplication().delegate as! AppDelegate).days = days
        
        let daysRow = (UIApplication.sharedApplication().delegate as! AppDelegate).daysRow
        var act = (UIApplication.sharedApplication().delegate as! AppDelegate).days[daysRow].activities.allObjects as! [Activity]
        let activitiesRow = (UIApplication.sharedApplication().delegate as! AppDelegate).activityRow
        participants = act[activitiesRow].participants.allObjects as! [Participant]
        
        row = (UIApplication.sharedApplication().delegate as! AppDelegate).participantsRow
        
        nameLBL.text = participants[row].name
        emailLBL.text = participants[row].email
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    
    @IBAction func sendEmailBTN(sender: AnyObject) {
        if (MFMailComposeViewController.canSendMail()) {
            
            let emailTitle = "Item check list"
            var messageBody = "The checked item list\n"
            for item in  appDelegate.items{
                if(item.checked){
                    messageBody += item.name + "\n"
                }
            }
            let mc : MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setToRecipients([emailLBL.text!] as [String])
            self.presentViewController(mc, animated: true, completion: nil)
        }
        else {
            UIAlertView(title: "Message", message: "Please add an email account in settings and come back!!", delegate: self, cancelButtonTitle: "Okay").show()
        }
        
    }
    
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        
        switch result.rawValue {
            
        case MFMailComposeResultCancelled.rawValue:
            print("Mail Cancelled")
        case MFMailComposeResultSaved.rawValue:
            print("Mail Saved")
        case MFMailComposeResultSent.rawValue:
            UIAlertView(title: "Message", message: "Email Sent Successfully :)", delegate: self, cancelButtonTitle: "Okay").show()
            print("Mail Sent")
        case MFMailComposeResultFailed.rawValue:
            print("Mail Failed")
        default:
            break
            
        }
    
        self.dismissViewControllerAnimated(false, completion: nil)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
