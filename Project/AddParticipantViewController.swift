//
//  AddParticipantViewController.swift
//  Project
//
//  Created by Student on 4/16/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit
import CoreData

class AddParticipantViewController: UIViewController,UITextFieldDelegate {
    
    var moc : NSManagedObjectContext!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var email: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addBTN(sender: AnyObject) {
        if nameTF.text == "" || email.text == "" {
            UIAlertView(title: "Warning", message: "Please enter all feilds", delegate: nil, cancelButtonTitle: "Okay").show()
        }
        else {
            var activityRow = (UIApplication.sharedApplication().delegate as! AppDelegate).activityRow
            moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
            
            var participant : Participant = NSEntityDescription.insertNewObjectForEntityForName("Participant", inManagedObjectContext: moc) as! Participant
            participant.name = NSString(string: nameTF.text!).capitalizedString
            participant.email = email.text!
            
            var fetch = NSFetchRequest(entityName: "Day")
            fetch.sortDescriptors = [NSSortDescriptor(key: "sortKey", ascending: true)]
            var days = (try! moc.executeFetchRequest(fetch)) as! [Day]
            (UIApplication.sharedApplication().delegate as! AppDelegate).days = days
            var daysRow = (UIApplication.sharedApplication().delegate as! AppDelegate).daysRow
            var act = (UIApplication.sharedApplication().delegate as! AppDelegate).days[daysRow].activities.allObjects as! [Activity]
            (UIApplication.sharedApplication().delegate as! AppDelegate).activities = act
            participant.activit = act[activityRow]
            
            var participants : [Participant] = (UIApplication.sharedApplication().delegate as! AppDelegate).activities[activityRow].participants.allObjects as! [Participant]
            participants.append(participant)
            (UIApplication.sharedApplication().delegate as! AppDelegate).activities[activityRow].participants = NSSet(array: participants)
            
            var error:NSError?
            do {
                try moc.save()
            } catch var error1 as NSError {
                error = error1
            }
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func cancleBTN(sender: AnyObject) {
        self.dismissViewControllerAnimated(true , completion: nil)
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
