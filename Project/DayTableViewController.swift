//
//  DayTableViewController.swift
//  Project
//
//  Created by Student on 4/15/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit
import CoreData

class DayTableViewController: UITableViewController {

    var moc : NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        var fetch = NSFetchRequest(entityName: "Day")
        fetch.sortDescriptors = [NSSortDescriptor(key: "sortKey", ascending: true)]
        var error : NSError?
        var days = (try! moc.executeFetchRequest(fetch)) as! [Day]
        if days.count == 0 {
            publishDatabase()
        }
        self.tableView.separatorColor = UIColor.blackColor()
        //self.tableView.backgroundColor = UIColor(patternImage: UIImage(named: "bg3.jpg")!)
        
        fetch = NSFetchRequest(entityName: "Day")
        fetch.sortDescriptors = [NSSortDescriptor(key: "sortKey", ascending: true)]
        days = (try! moc.executeFetchRequest(fetch)) as! [Day]
        (UIApplication.sharedApplication().delegate as! AppDelegate).days = days
      }
    
   
    
    func publishDatabase(){
        
        let day1 = NSEntityDescription.insertNewObjectForEntityForName("Day", inManagedObjectContext: moc) as! Day
        day1.name = "Sunday"
        day1.sortKey = "a"
        
        let day2 = NSEntityDescription.insertNewObjectForEntityForName("Day", inManagedObjectContext: moc) as! Day
        day2.name = "Monday"
        day2.sortKey = "b"
        
        let day3 = NSEntityDescription.insertNewObjectForEntityForName("Day", inManagedObjectContext: moc) as! Day
        day3.name = "Tuesday"
        day3.sortKey = "c"
        
        let day4 = NSEntityDescription.insertNewObjectForEntityForName("Day", inManagedObjectContext: moc) as! Day
        day4.name = "Wednesday"
        day4.sortKey = "d"
        
        let day5 = NSEntityDescription.insertNewObjectForEntityForName("Day", inManagedObjectContext: moc) as! Day
        day5.name = "Thursday"
        day5.sortKey = "e"
        
        let day6 = NSEntityDescription.insertNewObjectForEntityForName("Day", inManagedObjectContext: moc) as! Day
        day6.name = "Friday"
        day6.sortKey = "f"
        
        let day7 = NSEntityDescription.insertNewObjectForEntityForName("Day", inManagedObjectContext: moc) as! Day
        day7.name = "Saturday"
        day7.sortKey = "g"
        
        var error : NSError?
        
        do {
            try moc.save()
        } catch let error1 as NSError {
            error = error1
        }
        
    }
    
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        let fetch = NSFetchRequest(entityName: "Day")
        fetch.sortDescriptors = [NSSortDescriptor(key: "sortKey", ascending: true)]
        var error : NSError?
        
        let days = (try! moc.executeFetchRequest(fetch)) as! [Day]
        
        return days.count
    }

    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("day", forIndexPath: indexPath) 
        let DAY_TAG : Int = 100
        let title:UILabel = cell.viewWithTag(DAY_TAG) as! UILabel!
        var r = CGFloat(arc4random() % 255)
        var g = CGFloat(arc4random() % 255)
        var b = CGFloat(arc4random() % 255)
        var alpha = CGFloat(arc4random() % 100)
        cell.backgroundColor = UIColor(red: r/255, green: g/255, blue: b/255, alpha: 0.2)
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayOfWeekString = dateFormatter.stringFromDate(date)
        
        title.text = (UIApplication.sharedApplication().delegate as! AppDelegate).days[indexPath.row].name
        title.font = UIFont(name: "HoeflerText-Black", size: 20.0)
        title.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.7)
        if dayOfWeekString == (UIApplication.sharedApplication().delegate as! AppDelegate).days[indexPath.row].name {
            title.text = title.text! + "- Today"
            title.textColor = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 0.6)
            r = CGFloat(arc4random() % 255)
            g = CGFloat(arc4random() % 255)
            b = CGFloat(arc4random() % 255)
            title.shadowColor = UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1.0)
            
            title.font = UIFont(name: "MarkerFelt-Wide", size: 29.0)
         }
        
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        (UIApplication.sharedApplication().delegate as! AppDelegate).activities = (UIApplication.sharedApplication().delegate as! AppDelegate).days[indexPath.row].activities.allObjects as! [Activity]
        
        (UIApplication.sharedApplication().delegate as! AppDelegate).daysRow = indexPath.row
        
        
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
