//
//  Day.swift
//  Project
//
//  Created by Student on 4/15/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import Foundation
import CoreData



class Day: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var sortKey: String
    @NSManaged var activities: NSSet

}
