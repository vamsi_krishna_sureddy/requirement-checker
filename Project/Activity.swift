//
//  Activity.swift
//  Project
//
//  Created by Student on 4/15/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import Foundation
import CoreData


class Activity: NSManagedObject {

    @NSManaged var title: String
    @NSManaged var time: NSDate
    @NSManaged var day: NSManagedObject
    @NSManaged var items: NSSet
    @NSManaged var participants: NSSet

}
