//
//  ItemsAndParticipantsViewController.swift
//  Project
//
//  Created by Student on 4/15/15.
//  Copyright (c) 2015 Student. All rights reserved.
//

import UIKit
import CoreData

class ItemsAndParticipantsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var moc : NSManagedObjectContext!
    var items : [Item] = []
    var participants : [Participant] = []
    
    @IBOutlet weak var itemsTV: UITableView!
    
    @IBOutlet weak var participantsTV: UITableView!
    
    override func viewDidLoad() {
        
        //self.itemsTV.backgroundColor = UIColor(patternImage: UIImage(named: "bg.jpg")!)
        //self.participantsTV.backgroundColor = UIColor(patternImage: UIImage(named: "bg.jpg")!)
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg3.jpg")!)
        moc = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        super.viewDidLoad()
        self.itemsTV.separatorColor = UIColor.blackColor()
        self.participantsTV.separatorColor = UIColor.blackColor()
        let fetch = NSFetchRequest(entityName: "Day")
        fetch.sortDescriptors = [NSSortDescriptor(key: "sortKey", ascending: true)]
        let days = (try! moc.executeFetchRequest(fetch))as! [Day]  
        (UIApplication.sharedApplication().delegate as! AppDelegate).days = days
        
        let daysRow = (UIApplication.sharedApplication().delegate as! AppDelegate).daysRow
        var act = (UIApplication.sharedApplication().delegate as! AppDelegate).days[daysRow].activities.allObjects as! [Activity]
        let activitiesRow = (UIApplication.sharedApplication().delegate as! AppDelegate).activityRow
        items = act[activitiesRow].items.allObjects as! [Item]
        participants = act[activitiesRow].participants.allObjects as! [Participant]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == itemsTV {
            return items.count
        }
        else {
            return participants.count
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView == itemsTV {
            let cell = tableView.dequeueReusableCellWithIdentifier("item", forIndexPath: indexPath) 
            cell.backgroundColor = UIColor(red: 1/255, green: 1/255, blue: 1/255, alpha: 0.0)
            if items.count != 0 {
                cell.textLabel?.text = items[indexPath.row].name
                if items[indexPath.row].checked == true {
                    cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
                else {
                    cell.accessoryType = UITableViewCellAccessoryType.None
                }
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("participant", forIndexPath: indexPath) 
            cell.backgroundColor = UIColor(red: 1/255, green: 1/255, blue: 1/255, alpha: 0.0)
            if participants.count != 0 {
                cell.textLabel?.text = participants[indexPath.row].name
            }
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == itemsTV {
            let cell: UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
            let data = cell.textLabel?.text
            let fetchRequest = NSFetchRequest(entityName:"Item")
            fetchRequest.predicate = NSPredicate(format: "name=%@", data!)
            var error : NSError?
            var result = (try! moc.executeFetchRequest(fetchRequest)) as! [Item]
            
            if cell.accessoryType == UITableViewCellAccessoryType.None {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                result[0].setValue(true , forKey: "checked")
                do {
                    try moc.save()
                } catch _ {
                }
                items[indexPath.row].checked = true
            } else {
                cell.accessoryType = UITableViewCellAccessoryType.None
                result[0].setValue(false , forKey: "checked")
                do {
                    try moc.save()
                } catch _ {
                }
                items[indexPath.row].checked = false
            }
            (UIApplication.sharedApplication().delegate as! AppDelegate).items = items
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
        else {
            (UIApplication.sharedApplication().delegate as! AppDelegate).participantsRow = indexPath.row
         }
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == itemsTV {
            return "Items CheckList"
        }
        else {
           return  "Participants List"
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        let fetch = NSFetchRequest(entityName: "Day")
        fetch.sortDescriptors = [NSSortDescriptor(key: "sortKey", ascending: true)]
        let days = (try! moc.executeFetchRequest(fetch)) as! [Day]
        (UIApplication.sharedApplication().delegate as! AppDelegate).days = days
        
        let daysRow = (UIApplication.sharedApplication().delegate as! AppDelegate).daysRow
        var act = (UIApplication.sharedApplication().delegate as! AppDelegate).days[daysRow].activities.allObjects as! [Activity]
        let activitiesRow = (UIApplication.sharedApplication().delegate as! AppDelegate).activityRow
        items = act[activitiesRow].items.allObjects as! [Item]
        (UIApplication.sharedApplication().delegate as! AppDelegate).items = items
        self.navigationItem.title = act[activitiesRow].title
        participants = act[activitiesRow].participants.allObjects as! [Participant]
        (UIApplication.sharedApplication().delegate as! AppDelegate).participants = participants
        self.itemsTV.reloadData()
        self.participantsTV.reloadData()

    }
    
     func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == itemsTV {
            if editingStyle == UITableViewCellEditingStyle.Delete {
                moc.deleteObject(items[indexPath.row])
                items.removeAtIndex(indexPath.row)
                
                do {
                    try moc.save()
                } catch _ {
                }
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            }
        }
        else {
            if editingStyle == UITableViewCellEditingStyle.Delete {
                moc.deleteObject(participants[indexPath.row])
                participants.removeAtIndex(indexPath.row)
                
                do {
                    try moc.save()
                } catch _ {
                }
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            }
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
